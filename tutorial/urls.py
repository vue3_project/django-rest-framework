
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
#from tutorial.quickstart import views

# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
]


urlpatterns += [
    #path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

from rest_framework.routers import DefaultRouter
from inventory import views

router = DefaultRouter()
router.register(r'inventory', views.InventoryViewSet)
urlpatterns += [
    path('api/', include(router.urls),name="api"),
]

urlpatterns += [
    path('inventory/', views.InventoryList, name="inventory"),
    path('inventory/<int:id>', views.InvertoryDetail, name="inventory_id"),  
]
