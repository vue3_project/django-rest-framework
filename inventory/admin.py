from django.contrib import admin

# Register your models here.
from .models import Inventory, Supplier

#admin.site.register(Inventory)
# Define the admin class
class InventoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'note', 'stock', 'availability', 'supplier')
# Register the admin class with the associated model
admin.site.register(Inventory, InventoryAdmin)
admin.site.register(Supplier)


