

#from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from inventory.models import Inventory
from inventory.serializers import InventorySerializer

#client = APIClient()
#response1 = client.get('/api/inventory/')

class InventoryAPITest(APITestCase):
    def test_my_api(self):
        url = '/api/inventory/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)

        
class InventoryInventoryTest(APITestCase):
    def test_my_api_endpoint(self):
        url = '/inventory/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)



class InventoryDetailViewTestCase(TestCase):
    #Create in inventory object for testing
    def setUp(self):
        self.inventory = Inventory.objects.create(
            name='ITEM10',
            description='ITEM10 description',
            note='ITEM10 note',
            stock=10,
            availability=True,
        )
        self.client = Client()

    #Testing GET method
    def test_get_inventory_detail(self):
        url = reverse('inventory_id', args=[self.inventory.id])
        response = self.client.get(url)
        inventory = Inventory.objects.get(pk=self.inventory.id)
        serializer = InventorySerializer(inventory)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)


