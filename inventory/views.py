from django.shortcuts import render

# Create your views here.
from .models import Inventory, Supplier
from .serializers import InventorySerializer, SupplierSerializer, InventoryAPISerializer
from rest_framework import viewsets, status
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django_filters import rest_framework



# Create your views here.
class InventoryViewSet(viewsets.ModelViewSet):
    queryset = Inventory.objects.all()
    serializer_class = InventoryAPISerializer
    filter_backends = (rest_framework.DjangoFilterBackend,)
    filterset_fields = ['name']
   

class SupplierViewSet(viewsets.ModelViewSet):
    queryset = Supplier.objects.all()
    serializer_class = SupplierSerializer


@api_view(['GET', 'POST'])
def InventoryList(request):

    if request.method == 'GET':
        inventorys = Inventory.objects.all()        
        serializer = InventorySerializer(inventorys, many=True)       
        #return JsonResponse({'inventorys': serializer.data})        
        return Response(serializer.data)
    
    if request.method == 'POST':
        serializer = InventorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['GET', 'PUT', 'DELETE'])
def InvertoryDetail(request, id):

    try:
        inventory = Inventory.objects.get(pk=id)
    except Inventory.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    if request.method == 'GET':
        serializer = InventorySerializer(inventory)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = InventorySerializer(inventory,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        inventory.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    

