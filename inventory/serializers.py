from django.utils.timezone import now
from rest_framework import serializers
from inventory.models import Inventory, Supplier



class ToUpperCaseCharField(serializers.CharField):
    def to_representation(self, value):
        return value.upper()


class InventoryAPISerializer(serializers.ModelSerializer):    
    name = ToUpperCaseCharField()

    class Meta:
        model = Inventory
        # fields = '__all__'
        fields = ('id', 'name', 'availability', 'supplier')
    

class InventorySerializer(serializers.ModelSerializer):
    days_since_created = serializers.SerializerMethodField()
    name = ToUpperCaseCharField()

    class Meta:
        model = Inventory
        # fields = '__all__'
        fields = ('id', 'name', 'description', 'note', 'stock', 'availability', 'supplier', 'last_modify_date', 'created', 'days_since_created')

    def get_days_since_created(self, obj):
        return (now() - obj.created).days
    
class SupplierSerializer(serializers.ModelSerializer):
    days_since_created = serializers.SerializerMethodField()
    name = ToUpperCaseCharField()

    class Meta:
        model = Supplier
        # fields = '__all__'
        fields = ('id', 'supplier_name')