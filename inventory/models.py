from django.db import models


# Create your models here.
class Inventory(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    note = models.TextField()
    stock = models.IntegerField()
    availability = models.BooleanField()
    supplier = models.ForeignKey('Supplier', on_delete=models.SET_NULL, null=True)
    last_modify_date = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    # class Meta:
    #     db_table = "inventory"

    class Meta:
        ordering = ['name']

    #representing the Model object
    def __str__(self):       
        return f'{self.name}'


class Supplier(models.Model):
    supplier_name = models.CharField(max_length=100)
    

    # class Meta:
    #     db_table = "supplier"

    class Meta:
        ordering = ['supplier_name']


    # def get_absolute_url(self):
    #     return reverse('supplier-detail', args=[str(self.id)])
    
    #representing the Model object
    def __str__(self):
        return f'{self.supplier_name}'
